import java.io.File
import java.io.InputStream
import sun.misc.IOUtils

class waw(fileName : String, fileSamples : String, fileFFT : String) {
    init
    {
        val inputStream : InputStream = File(fileName).inputStream()
        val bytes : ByteArray = IOUtils.readFully(inputStream, -1, false)
        val bSize : Int = bytes.size
        var i : Int = 0
        println("( 1-4 ) ${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}")
        val overall_size : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8) or ( (bytes[i++].toInt() and 0xFF) shl 16) or ( (bytes[i++].toInt() and 0xFF) shl 24)
        println("( 5-8 ) Overall size: ${overall_size} bytes, ${overall_size / 1024} kbytes")
        println("( 9-12) Wave marker: ${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}")
        println("(13-16) Fmt marker: ${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}")
        val length_of_fmt : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8) or ( (bytes[i++].toInt() and 0xFF) shl 16) or ( (bytes[i++].toInt() and 0xFF) shl 24)
        println("(17-20) Length of Fmt header: ${length_of_fmt}")
        val format_type : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF)  shl 8)
        print("(21-22) Format type: ${format_type} ")
        if (format_type == 1)
        {
            println("PCM")
        }
        else if (format_type == 6)
        {
            println("A-law")
        }
        else if (format_type == 7)
        {
            println("Mu-law")
        }
        val channels : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8)
        println("(23-24) Channels: ${channels}")
        val sample_rate : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8) or ( (bytes[i++].toInt() and 0xFF) shl 16) or ( (bytes[i++].toInt() and 0xFF) shl 24)
        println("(25-28) Sample rate: ${sample_rate}")
        val byterate : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8) or ( (bytes[i++].toInt() and 0xFF) shl 16) or ( (bytes[i++].toInt() and 0xFF) shl 24)
        println("(29-32) Byte Rate: ${byterate}, Bit Rate: ${byterate * 8}")
        val block_align : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8)
        println("(33-34) Block Alignment: ${block_align}")
        val bits_per_sample : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8)
        println("(35-36) Bits per sample: ${bits_per_sample}")
        println("(37-40) Data Marker: ${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}${bytes[i++].toChar()}")
        val data_size : Int
                = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8) or ( (bytes[i++].toInt() and 0xFF) shl 16) or ( (bytes[i++].toInt() and 0xFF) shl 24)
        println("(41-44) Size of data: ${data_size}\n")
        println("-------------------- Samples Info --------------------")
        val num_samples : Long = ( 8 * data_size.toLong() ) / (channels * bits_per_sample)
        println("Number of samples: ${num_samples}")
        val size_of_each_sample : Long = (channels.toLong() * bits_per_sample) / 8
        println("Size of each sample: ${size_of_each_sample} bytes")
        val duration_in_seconds : Float = overall_size.toFloat() / byterate
        println("Approx.Duration in seconds = ${duration_in_seconds}")
        //println("Approx.Duration in h:m:s = %s\n", seconds_to_time(duration_in_seconds))

        var size_is_correct : Boolean = true

        // make sure that the bytes-per-sample is completely divisible by num.of channels
        val bytes_in_each_channel : Long = size_of_each_sample / channels
        if ( (bytes_in_each_channel * channels) != size_of_each_sample )
        {
            println("Error: ${bytes_in_each_channel} x ${channels} <> ${size_of_each_sample}")
            size_is_correct = false
        }


        if (size_is_correct)
        {
            // the valid amplitude range for values based on the bits per sample
            var low_limit : Long = 0
            var high_limit : Long = 0

            if (bits_per_sample == 8) {
                low_limit = -128
                high_limit = 127
            } else if (bits_per_sample == 16) {
                low_limit = -32768
                high_limit = 32767
            } else if (bits_per_sample == 32) {
                low_limit = -2147483648
                high_limit = 2147483647
            }

            println("Valid range for data values : ${low_limit} to ${high_limit}\n")

            // Window
            val window_length : Int = 1024
            val data = DoubleArray(window_length)
            var k : Int = 0
            println("Length window : ${window_length} \n")

            val mfft = fft()
            File(fileSamples).createNewFile()
            File(fileFFT).createNewFile()
            var l : Long = 0
            while (l <= num_samples)
            {
                // dump the data read
                var xchannels : Int = 0
                var data_in_channel : Int = 0

                while (xchannels < channels)
                {
                    // Convert data from little endian to big endian based on bytes in each channel sample
                    if( (bytes_in_each_channel.toInt() == 4) and ( (i + 4) < bSize ) )
                    {
                        data_in_channel = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8) or ( (bytes[i++].toInt() and 0xFF) shl 16) or ( (bytes[i++].toInt() and 0xFF) shl 24)
                    }
                    else if( (bytes_in_each_channel.toInt() == 2) and ( (i + 2) < bSize ) )
                    {
                        data_in_channel = (bytes[i++].toInt() and 0xFF) or ( (bytes[i++].toInt() and 0xFF) shl 8)
                    }
                    else if( (bytes_in_each_channel.toInt() == 1) and (i < bSize) )
                    {
                        data_in_channel = bytes[i++].toInt()
                    } else break

                    // write to file
                    if( xchannels == 0 )
                    {
                        data[k] = data_in_channel.toDouble()
                        if( k < (window_length - 1) )
                        {
                            k++
                        }
                        else
                        {
                            k = 0
                            mfft.difftt( data, window_length, fileFFT, sample_rate )
                        }
                        File(fileSamples).appendText("${data_in_channel.toDouble()}\n")
                    }

                    // check if value was in range
                    if( (data_in_channel < low_limit) or (data_in_channel > high_limit) )
                    {
                        println("**value out of range")
                    }
                    xchannels++
                }
                l++
            }
        }
    }

}