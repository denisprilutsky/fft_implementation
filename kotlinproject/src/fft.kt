import java.lang.Math.*
import java.io.File

class fft {
    private fun window(data : DoubleArray, len : Int) : DoubleArray
    {
        // hann window
        var i : Int = 0
        while (i < len)
        {
            data[i] = 0.5 * ( 1 - cos( 2 * PI * i / (len - 1) ) ) * data[i]
            i++
        }
        return data
    }

    fun difftt( in_data : DoubleArray, len : Int, fp : String, sample_rate : Int )
    {
        // Window transformation
        val data : DoubleArray = window(in_data, len)

        var wtemp : Double
        var wr : Double
        var wpr : Double
        var wpi : Double
        var wi : Double
        var theta : Double

        var tempr : Double
        var tempi : Double

        val N : Int = len/2
        var i : Int
        var j : Int
        val n : Int = N * 2
        var m : Int
        var istep : Int
        var mmax : Int = n/2

        // calculate the FFT
        while(mmax >= 2)
        {
            istep = (mmax and 0xFFFFFF) shl 1
            theta = 1.0 * (2 * PI / mmax)
            wtemp = 1.0 * sin(0.5 * theta)
            wpr = -2.0 * wtemp * wtemp

            wpi = sin(theta)

            wr = 1.0
            wi = 0.0

            m = 1
            while(m < mmax)
            {
                i = m
                while(i < n)
                {
                    j = i + mmax - 1

                    //println("${data[i]},${data[i+1]},${i},${j},${data.size}\n")
                    //println("${data[j]},${data[j+1]},${i},${j},${data.size}\n")
                    tempr = data[i]
                    tempi = data[i+1]

                    data[i] = data[i] + data[j]
                    data[i+1] = data[i+1] + data[j+1]

                    data[j] = tempr - data[j]
                    data[j+1] = tempi - data[j+1]

                    tempr = wr * data[j] - wi * data[j+1]
                    tempi = wr * data[j+1] + wi * data[j]

                    data[j] = tempr
                    data[j+1] = tempi
                    i += istep
                }
                wtemp = wr
                wr += wtemp * wpr - wi * wpi
                wi += wtemp * wpi + wi * wpr
                m += 2
            }
            mmax /= 2
        }
        // bit-reversal
        j = 1
        i = 1
        while(i < n)
        {
            if (j > i)
            {
                data[i] = data[j].also { data[j] = data[i] }
                data[i+1] = data[j+1].also { data[j+1] = data[i+1] }
            }
            m = (n and 0xFF) ushr 1
            while( (m >= 2) and (j > m) )
            {
                j -= m
                m = (m and 0xFF) ushr 1
            }
            j += m
            i += 2
        }

        // write to file

        var k = 0
        while( k < (2 * N) )
        {
            j = k/2
            val real_ : Double = j.toDouble() * sample_rate.toDouble() / N
            val imag_ : Double = 2.0 * sqrt( data[k] * data[k] + data[k+1] * data[k+1] ) / N
            //println("${real_},${imag_}\n")
            File(fp).appendText("${real_},${imag_}\n")
            k += 2
        }
    }

}