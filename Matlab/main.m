%% Read FFT data
data = dlmread( sprintf('../kotlinproject/fft.data') );
data = data';
window = 1024;
%% Number of window in FFT data
N = fix( length( data(1,:) )/window );

%% FFT 1
j = 1;
for i=1:1:N
    Z(i, :) = data(2, j:(j + window/2 - 1) );
    j = j + window;
end
figure(1)
[X, Y] = meshgrid(1:1:window, 1:1:N);
X( 1:length( data(:,1) ) ) = data(:, 1);
mesh(Z)

%% FFT 2 for comparison
data = dlmread( sprintf('../kotlinproject/samples_waw.data') );
figure(2)
plot( 1:(length(data)/2 - 1), fft(data(1:(length(data)/2 - 1))) );
